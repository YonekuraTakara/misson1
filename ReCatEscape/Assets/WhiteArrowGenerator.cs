﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteArrowGenerator : MonoBehaviour
{

    public GameObject WhiteArrowPrefab;
    float span = 1.0f;
    float delta = 0;
    float Up = 0.9f;
    float speed = 10.0f;
    float Trigeer = 0.0f;


    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(WhiteArrowPrefab) as GameObject;
            float px = Random.Range(-4.0f, 5.0f);
            go.transform.position = new Vector3(speed, px, 0);

            Trigeer = Trigeer + 1.0f;

            if (Trigeer >= 10.0)
            {
                Trigeer = 0f;
                speed = speed / Up;
                span = span * Up;
            }
        }
    }
}
