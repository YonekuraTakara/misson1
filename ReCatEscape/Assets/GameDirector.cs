﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameDirector : MonoBehaviour
{
    // Start is called before the first frame update

    public static int Co = 0;
    public static int Tr = 0;
    public int Death = 0;
    public GameObject hpGauge;
    public GameObject arrowCount = null;

    void Start()
    {
        this.hpGauge = GameObject.Find("hpGauge");
        Co = 0;
        Death = 0;

    }

    public void DecreaseHp()
    {
        this.hpGauge.GetComponent<Image>().fillAmount -= 0.1f;
        Death++;
    }

    public void DecreaseCo()
    {
        Co++;
    }

    void Update()
    {
        //矢のカウント
        Text arrowText = this.arrowCount.GetComponent<Text>();
        arrowText.text = "×" + Co;

        //ゲームオーバー判定
        if (Death >= 10)
        {
            SceneManager.LoadScene("GameOverScene");
        }

        if (Co % 10 == 0)
        {
            Tr++;
        }
    }

    public static int getScore()
    {
        return Co;
    }
}