﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedArrowGenerator : MonoBehaviour
{

    public GameObject RedArrowPrefab;
    float span = 1.0f;
    float delta = 0;


    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(RedArrowPrefab) as GameObject;
            float px = Random.Range(-4.0f, 5.0f);
            go.transform.position = new Vector3(10,px , 0);
        }
    }
}
