﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{

    public GameObject ScoreCount = null;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        int Result = GameDirector.getScore();

        Text ScoreText = ScoreCount.GetComponent<Text>();
        ScoreText.text = "今回の得点：" + Result;
    }
}
